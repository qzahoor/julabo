
import serial
import configparser
import mysql.connector
from datetime import datetime
import os
import logging

# Read the configuration file
config = configparser.ConfigParser()
config.read('Config2.ini')



# Get a list of all sections in the configuration file
sections = config.sections()

# Print the list of sections
print(sections)

# Get the serial port configuration
#port = config['serial']['port']
#baudrate = int(config['serial']['baudrate'])
#timeout = int(config['serial']['timeout'])


# get values from environment variables
host = os.environ.get('MYSQL_HOST', config['mysql']['host'])
database = os.environ.get('MYSQL_DATABASE', config['mysql']['database'])
user = os.environ.get('MYSQL_USER', config['mysql']['user'])
password = os.environ.get('MYSQL_PASSWORD', config['mysql']['password'])
port = os.environ.get('MYSQL_PORT', config['mysql']['port'])



# Connect to the serial port
ser = serial.Serial('COM10', 9600, timeout=1)

def open_db():
    try:
        mydb = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            port=port,
            database=database
        )
        logging.info('Connected to database')
    except mysql.connector.Error as err:
        logging.error('Could not connect to database: {}'.format(err))
        mydb = None
    return mydb

def epoch():
    tstamp = int(round(datetime.now().timestamp()))
    return tstamp


print("Connected to databse and Julabo A80")


# Get the measurement types from the database
mycursor = mydb.cursor()

mycursor.execute("SELECT id, name FROM measurement_types WHERE id IN (22, 23)")
measurement_types = {row[1]: row[0] for row in cursor.fetchall()}


# Turn on the chiller
ser.write("out_mode_05 1\r\n".encode("utf-8"))
time.sleep(6)











# Set the setpoint array
for temp in range(18, 22):
    ser.write(f'set_point={temp}\r'.encode())
    time.sleep(300) # Wait for 5 minutes
    bath_temp = ser.readline().decode().strip()
    setpoint_temp = temp
    # Insert the measurements into the database
    mycursor.execute("INSERT INTO measurements (value, measurement_type_id) VALUES (%s, %s)", (setpoint_temp, measurement_types['setpoint_temp']))
    mycursor.execute("INSERT INTO measurements (value, measurement_type_id) VALUES (%s, %s)", (bath_temp, measurement_types['Bath_temperature']))
    mydb.commit()

# Turn off the chiller
ser.write("out_mode_05 0\r\n".encode("utf-8"))

# Close the serial port
ser.close()

# Close the database connection
mydb.close()

