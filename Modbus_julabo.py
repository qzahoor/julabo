from configparser import ConfigParser
import mysql.connector
from datetime import datetime
import time
from pyModbusTCP.client import ModbusClient
from pyModbusTCP import utils

# read configuration file
config = ConfigParser()
config.read('config.ini')


# Get the variables from the config file
# Database write access
dbHost = config['mysql']['host']
dbPort = config['mysql']['port']
dbDatabase = config['mysql']['database']
dbUser = config['mysql']['user']
dbPassword = config['mysql']['password']

# connect to database
mydb = mysql.connector.connect(
    host=dbHost,
    port=dbPort,
    user=dbUser,
    password=dbPassword,
    database=dbDatabase
)

print(mydb, "is connected")

# Create a cursor object
mycursor = mydb.cursor()

def epoch():
    epoch_timestamp = int(round(datetime.now().timestamp()))
    return epoch_timestamp


measurement_types = {'setpoint_temp': 1, 'bath_temp': 2}
##Connect to the Julabo

try:
    c = ModbusClient(host='hostname', port=502, auto_open=True, auto_close=True)
    
except ValueError:
    print("Error with host or port params")

if c.open():
    running_list  = c.read_holding_registers(0, 1) # This is a 16-bit integer holding 0 or 1
    setpoint_temp_list = c.read_holding_registers(2, 2) # These are two 16-bit registers holding a float
    
    bath_temp_list = c.read_input_registers(10, 2) #to read the bath_temp


    print("Connected to Modbus TCP/IP server")
    

running  = running_list[0]
setpoint_temp = utils.decode_ieee(utils.word_list_to_long(setpoint_temp_list)[0]) # We need to decode two 16-bit integers -> float
##If am getting issuue here beacuse i get value equals to zero when i try with these two 


bath_temp = utils.decode_ieee(utils.word_list_to_long(bath_temp_list)[0])




mycursor.execute("SELECT id, name FROM measurement_types WHERE id IN (22, 23)")
measurement_types = {row[1]: row[0] for row in mycursor.fetchall()}
##If the Chiller is ON 

if (running == 1):
    print("The chiller is ", end='')
    print("running, and the setpoint is {:.4f}".format(setpoint_temp)+" C")
    print("bath_temp is {:.2f}".format(bath_temp)+ " C")

   
##If the Chiller is Off 
    
if (running == 0):
    print("not ", end='')
    
    #setpoint_temp = 0.99  # the value column of database is set not null so we should alter the table if we wants to store null
    #bath_temp = 0.99
print("running, and the setpoint is {:.4f}".format(setpoint_temp)+" C")
print("bath_temp is {:.2f}".format(bath_temp)+ " C")









# Insert the measurements into the database, including the epoch timestamp
epoch_timestamp = epoch()
mycursor.execute("INSERT INTO measurements (value, measurement_type_id, epoch_timestamp) VALUES (%s, %s, %s)", (setpoint_temp, measurement_types['setpoint_temp'], epoch_timestamp))
mycursor.execute("INSERT INTO measurements (value, measurement_type_id, epoch_timestamp) VALUES (%s, %s, %s)", (bath_temp, measurement_types['Bath_temp'], epoch_timestamp))
mydb.commit()

# Close the Modbus TCP/IP connection and database connection

mycursor.close()
c.close()



